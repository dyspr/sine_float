var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var parameters = [
  [0.0455, 0.079, 0.105, 0.90],
  [0.0325, 0.056, 0.112, 0.80],
  [0.0475, 0.063, 0.107, 0.95]
]
var numOf = 128

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < parameters.length; j++) {
    for (var i = numOf; i > 0; i--) {
      fill(32 + (255 - 32) * abs(sin(frameCount * 0.05 + i * parameters[j][0])))
      noStroke()
      push()
      translate(windowWidth * 0.5 + ((numOf - i) / numOf) * sin(frameCount * 0.04 + i * parameters[j][1] + j) * boardSize * 0.0666 - (j - 1) * boardSize * 0.25, windowHeight * 0.5 + (i - Math.floor(numOf * 0.5)) * boardSize * 0.005 * (1 / (numOf / 128)))
      ellipse(0, 0, boardSize * 0.05 * (sin(frameCount * 0.1 + i * parameters[j][2])) + (Math.floor(numOf * 0.5) - i - j) * boardSize * 0.001 * parameters[j][3])
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
